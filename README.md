# LorProject2 Readme (In-Progress)

The main purpose of this python program is to simulate Airport take-off time. 
We will need to keep track of a few key points to schedule the airstrip resource: 
The Request Identifier, Request Submission Time, and Length of Time Requested. 
We will be able to do this either by using one of the built-in types, or by creating a class to hold all the info.
From there, we should be able to select a .txt file and print out the actual take-off times.
